package sog.toolchain.utility.enums;

/**
 * Types of an entity
 * @see java.lang.Enum
 * @options
 * @opt Alive
 * @opt Item
 * @opt Placeable
 * @opt Interactive
 * @opt Environmental
 */
public enum EntityTypes {
        /**
         * A living entity. Has health, can be attacked
         */
        ALIVE,
        /**
         * An item entity. Can be used and destroyed by throwing into cactus
         */
        ITEM,
        /**
         * A block / other (fence / door) placeable entity. Can be destroyed and picked up.
         */
        PLACEABLE,
        /**
         * An entity that can be interacted with, yet cannot be attacked, used or destroyed.
         */
        INTERACTIVE,
        /**
         * A block or entity, that cannot be attacked, destroyed, used or placed.
         * #example Air
         */
        ENVIRONMENTAL
}
