package sog.toolchain.utility.enums;

public enum LoggerLevels {
    INFO,
    WARNING,
    ERROR
}
