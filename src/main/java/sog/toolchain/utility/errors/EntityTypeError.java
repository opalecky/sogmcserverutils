package sog.toolchain.utility.errors;

import sog.toolchain.utility.enums.EntityTypes;

public class EntityTypeError extends Error {
    public EntityTypeError(EntityTypes expected, EntityTypes got) {
        super("Expected " + expected.name() + " entity type but got entity type of " + got.name());
    }
}
