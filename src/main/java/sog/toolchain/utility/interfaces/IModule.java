package sog.toolchain.utility.interfaces;

public interface IModule<T> {
    String getName();

    T getModuleClass();

    boolean loadSettings();

    boolean updateSettings();

    boolean saveSettings();

     boolean saveData();

    boolean startModule();

    boolean stopModule();
}
