package sog.toolchain.utility.interfaces;

import com.google.common.primitives.UnsignedInteger;

import java.util.ArrayList;

/**
 *
 */
public interface ILivingEntity extends IEntity {

    /**
     *
     * @return
     */
    float getCurrentHealth();

    /**
     *
     * @param in_newHealth
     * @return
     */
    boolean setCurrentHealth(float in_newHealth);

    /**
     *
     * @return
     */
    float getMaxHealth();

    /**
     *
     * @return
     */
    float getBonusMaxHealth();

    /**
     *
     * @return
     */
    boolean setBonusMaxHealth();

    /**
     *
     * @return
     */
    ArrayList<IItem> retrieveItemsFromInventory();

    /**
     *
     * @param itemSlot
     * @return
     */
    boolean dropItem(UnsignedInteger itemSlot);
}
