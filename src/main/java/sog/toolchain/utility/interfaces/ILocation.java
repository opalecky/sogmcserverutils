package sog.toolchain.utility.interfaces;

public interface ILocation {
    double getX();
    double getY();
    double getZ();
    int getChunkX();
    int getChunkY();
    int getChunkZ();
    double getRelativeX();
    double getRelativeY();
    double getRelativeZ();
    double getClosestBlockCenterX();
    double getClosestBlockCenterY();
    double getClosestBlockCenterZ();
}
