package sog.toolchain.utility.interfaces;

import sog.toolchain.utility.enums.EntityTypes;

import java.util.UUID;

/**
 * Anything in the world is an entity and I want to track entities that are active
 * @exaplanation Active = Players, interactive to players and in players inventory. Placed block is no longer active unless specified anyway.
 */
public interface IEntity {
    /**
     *
     * @return
     */
    ILocation getLocation();

    /**
     *
     * @param inLocation
     * @return
     */
    boolean setLocation(ILocation inLocation);

    /**
     *
     * @return
     */
    UUID getUniqueId();

    /**
     *
     * @return
     */
    boolean isUsable();

    /**
     *
     * @return
     */
    boolean isPlacable();

    /**
     *
     * @return
     */
    boolean isAttackable();

    /**
     *
     * @return
     */
    boolean isThrowable();
}
