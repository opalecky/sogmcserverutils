package sog.toolchain.utility.interfaces;

import sog.toolchain.utility.enums.LoggerLevels;

/**
 * Logger interface
 * @important Should be a singleton
 */
public interface ILogger {

    boolean log(LoggerLevels level, String message);
    boolean info(String message);
    boolean warning(String message);
    boolean error(String message);
}
