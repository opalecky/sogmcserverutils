package sog.toolchain.utility.interfaces;

import java.util.ArrayList;

public interface IModuleLoader {
    ArrayList<IModule> getAllModules();
    IModule getModule(String moduleName);
    boolean addModule(IModule module);
    boolean disableModule(String name);

}
